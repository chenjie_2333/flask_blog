import sqlalchemy


engine = sqlalchemy.create_engine(url=f'mysql+mysqldb://root:123456@localhost:3306/flask-blog', echo=True)


def test():
    conn = engine.connect()
    print(conn)
    query = sqlalchemy.text('select * from articles')
    queryset = conn.execute(query)
    print(queryset.keys())
    for row in queryset:
        print(row)
    conn.close()
    engine.dispose()  # 关闭引擎


if __name__ == '__main__':
    test()