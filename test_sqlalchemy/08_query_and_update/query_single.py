from db_init import Session, Person


session = Session()

# person = session.query(Person).filter(Person.address == 'aaa').first()
person = session.query(Person).filter(Person.id == 100).first()  # 常用
# person = session.query(Person).filter(Person.id == 1).one()  # 要求查出来的结果有且只有1条，否则报错
# person = session.query(Person).filter(Person.id == 1).scalar()  # 查出来不止1条结果也会报错
print(person)
if person:
    print(f'name: {person.name}, birthday: {person.birthday}')
