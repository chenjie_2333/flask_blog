from db_init import Session, Person


session = Session()

# 方法1：通过修改对象的属性进行db的update
person = session.query(Person).filter(Person.id == 1).one()
person.address = 'wwww'

# 方法2：批量update
# session.query(Person).filter(Person.id > 14).update({
#     Person.address: 'Beijing'
# })


session.commit()
