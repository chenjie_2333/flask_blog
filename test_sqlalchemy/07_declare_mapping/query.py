from db_init import Session, Person


# keyPoint 通过session实现面向对象，操作数据库
# 使用session代替connection
session = Session()

# p = Person(name="Amy", birthday="2000-9-18", address="unknown")
# session.add(p)

# todo 表字段的更新应该如何通过sqlalchemy进行操作
person_list = [
    Person(name="Eric", birthday="1998-2-18", address="unknown"),
    Person(name="Samuel", birthday="1997-1-15", address="unknown")
]
session.add_all(person_list)

session.commit()
