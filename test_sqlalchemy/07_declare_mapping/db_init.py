from sqlalchemy import create_engine, Column, Integer, String, Date
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from test_sqlalchemy import engine

# engine = create_engine('mysql://root:test@localhost/testdb', echo=True)
"""
从07开始为映射类的学习，通过类来对db进行操作
总结sqlalchemy的两种使用方式：
1、connection 是通过原生sql进行操作
2、session 是通过python类对象进行映射操作
"""
# 映射类的基类
Base = declarative_base()


class Person(Base):
    __tablename__ = "person"

    id = Column(Integer, primary_key=True)
    name = Column(String(128), unique=True, nullable=False)
    birthday = Column(Date, nullable=False)
    address = Column(String(255), nullable=True)


# 新建表
Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine)
