from sqlalchemy import and_, or_

from db_init import engine, department, employee
import sqlalchemy

with engine.connect() as conn:
    join = employee.join(department, department.c.id == employee.c.department_id)
    query = sqlalchemy.select(join).where(department.c.name == 'hr')
    # query = sqlalchemy.select(employee).select_from(join).where(department.c.name == 'hr')
    # query = sqlalchemy.select(department).select_from(join).where(employee.c.name == 'Rose')
    print(conn.execute(query).fetchall())

    print("=================================上下两条sql等价")
    query = employee.join(department, department.c.id == employee.c.department_id).select().where(department.c.name == 'hr')
    # query = employee.join(
    #     department, or_(department.c.id == employee.c.department_id, department.c.id == 1)
    # ).select().where(or_(department.c.name == 'hr', department.c.name == 1))
    print(conn.execute(query).fetchall())

