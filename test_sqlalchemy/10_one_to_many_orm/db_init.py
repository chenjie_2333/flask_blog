import datetime

from sqlalchemy import create_engine, String, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, Mapped, mapped_column, relationship
from typing_extensions import Annotated
from typing import List

from test_sqlalchemy import engine
# engine = create_engine('mysql://root:test@localhost/testdb', echo=True)
Base = declarative_base()
"""
基于面向对象进行关系映射
"""


int_pk = Annotated[int, mapped_column(primary_key=True)]
required_unique_name = Annotated[str, mapped_column(String(128), unique=True, nullable=False)]
timestamp_not_null = Annotated[datetime.datetime, mapped_column(nullable=False)]


class Department(Base):
    __tablename__ = "department"

    id: Mapped[int_pk]
    name: Mapped[required_unique_name]

    employees: Mapped[List["Employee"]] = relationship(back_populates="department")  # back_populates List显示的定义”多“

    def __repr__(self):
        return f'id: {self.id}, name: {self.name}'


class Employee(Base):
    __tablename__ = "employee"

    id: Mapped[int_pk]
    department_id: Mapped[int] = mapped_column(ForeignKey("department.id"))
    name: Mapped[required_unique_name]
    birthday: Mapped[timestamp_not_null]
    address: Mapped[str] = mapped_column(String(128), nullable=True)

    # relationship 说明 department 这个字段非数据库中的字段，而是加载到内存的关系字段
    # lazy为懒加载，为True时（默认），只有当department这个属性被调用时，才会单独生成sql去查询； 为False时会将两张表join起来一起查询
    department: Mapped[Department] = relationship(lazy=False, back_populates="employees")  # back_populates 显示的定义”一“
    # keyPoint 两张表的relationship字段的定义互相关联

    # def __repr__(self):
    #     return f'id: {self.id}, dep_id: {self.department_id}, name: {self.name}, birthday: {self.birthday}'


Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine)
