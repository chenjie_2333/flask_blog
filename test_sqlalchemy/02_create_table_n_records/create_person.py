import sqlalchemy
from test_sqlalchemy import engine

# engine = sqlalchemy.create_engine('mysql://root:test@localhost/testdb', echo=True)

meta_data = sqlalchemy.MetaData()

person = sqlalchemy.Table(
    "person",
    meta_data,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("name", sqlalchemy.String(128), unique=True, nullable=False),
    sqlalchemy.Column("birthday", sqlalchemy.Date, nullable=False),
)


def create_table():
    meta_data.create_all(engine)


def insert_single():
    person_insert = person.insert()
    print(person_insert)
    insert_tom = person_insert.values(name="TomTom", birthday="2000-10-11")

    with engine.connect() as conn:
        result = conn.execute(insert_tom)
        print(result.is_insert)
        print(result.inserted_primary_key_rows)
        print(result.inserted_primary_key)
        conn.commit()


def insert_multi():
    person_insert = person.insert()
    print(person_insert)
    with engine.connect() as conn:
        result = conn.execute(person_insert, [
            {"name": "Jack", "birthday": "2000-10-13"},
            {"name": "Mary", "birthday": "2000-10-14"},
            {"name": "Smith", "birthday": "2000-10-15"},
        ])
        print(result.is_insert)
        print(result.inserted_primary_key_rows)  # [(None,), (None,), (None,)]
        # print(result.inserted_primary_key)  # 会报错
        conn.commit()


if __name__ == '__main__':
    # create_table()
    # insert_single()
    insert_multi()
