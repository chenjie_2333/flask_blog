from db_init import engine, person_table
from sqlalchemy.sql import and_, or_

with engine.connect() as conn:
    # query = person_table.select().where(person_table.c.birthday > '2000-10-13').where(person_table.c.id < 6)
    query = person_table.select().where(
        or_(
            # person_table.c.name == 'Tom',
            person_table.c.name == 'TomTom',
            and_(
                person_table.c.birthday > '2000-10-13',
                person_table.c.id < 7
            )
        )
    )
    print("打印sql：", str(query))
    result_set = conn.execute(query)
    # print(result_set.dialect)

    result = result_set.fetchall()
    print(result)
