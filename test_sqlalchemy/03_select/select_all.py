from db_init import engine, person_table

with engine.connect() as conn:
    query = person_table.select()
    result_set = conn.execute(query)

    # for row in result_set:
    #     print(row)
    #     print(row[0])
    #     print(row.id, row.name, row.birthday)
    # print("============================================")

    result = result_set.fetchall()
    print(result)
    for row in result:
        print(row)
        print(row[0])
        print(row.id, row.name, row.birthday)

    # row = result_set.fetchone()
    # print(row)
