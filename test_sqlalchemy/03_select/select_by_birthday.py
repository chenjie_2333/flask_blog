from db_init import engine, person_table

with engine.connect() as conn:
    # person_table.c  c指代column，为所有列的别名，等价于person_table.columns
    print('person_table.c:', person_table.c)
    print('person_table.columns:', person_table.columns)
    query = person_table.select().where(person_table.c.birthday > '2000-10-13')
    result_set = conn.execute(query)
    print(result_set)
    result = result_set.fetchall()
    print(result)
    _res = [dict(zip(result_set.keys(), row)) for row in result]
    print(_res)
