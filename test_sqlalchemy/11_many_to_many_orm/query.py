from db_init import Session, User, Role


def insert_records(session: Session):
    """这里看起来直接通过 add_all(user对象) ，就实现三张表的插入"""
    role1 = Role(name="Admin")
    role2 = Role(name="Operator")

    user1 = User(username="Jack", password="111")
    user2 = User(username="Tom", password="222")
    user3 = User(username="Mary", password="333")

    user1.roles.append(role1)
    user1.roles.append(role2)

    user2.roles.append(role1)
    user3.roles.append(role2)

    session.add_all([user1, user2, user3])

    session.commit()


def select_user(session: Session):
    u = session.query(User).filter(User.id == 2).one()
    print(u)
    print(u.roles)


def select_role(session: Session):
    r = session.query(Role).filter(Role.id == 7).one()
    print(r)
    print(r.users)


_session = Session()
# insert_records(_session)
# select_user(_session)
select_role(_session)
