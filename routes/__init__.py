import os

from flask import Flask
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy


MYSQL_HOST = os.getenv("MYSQL_HOST", "localhost")
MYSQL_PORT = os.getenv("MYSQL_PORT", "3306")
MYSQL_USER = os.getenv("MYSQL_USER", "root")
MYSQL_PWD = os.getenv("MYSQL_PWD", "123456")
MYSQL_DB = os.getenv("MYSQL_DB", "flask-blog")

# todo test sqlalchemy & 图片上传下载 & docker部署
# 启动服务: flask --app=routes run --debug -p 8080
# 进入shell的命令: flask --app=routes shell （默认是跟目录有一个app.py的文件，所以直接用flask shell）
app = Flask(__name__,
            template_folder='../templates',
            static_folder='../assets',
            static_url_path='/assets')
app.config['SQLALCHEMY_DATABASE_URI'] = f'mysql+mysqldb://{MYSQL_USER}:{MYSQL_PWD}@{MYSQL_HOST}:{MYSQL_PORT}/{MYSQL_DB}'
app.config['SECRET_KEY'] = 'ec9439cfc6c796ae2029594d'

db = SQLAlchemy(app)
login_manager = LoginManager(app)


from routes import user_routes
from routes import admin_routes
# todo test 下面关于ddl的迁移的操作待验证  https://cloud.tencent.com/developer/ask/sof/256318
# flask db init
# flask db migrate -m "Your Message."
# flask db upgrade
