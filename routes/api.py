import json

from flask import Flask, url_for, request, jsonify
from markupsafe import escape

app = Flask(__name__)


@app.route('/index/', methods=['get'])  # 路径最后加斜杆会将"/index"自动重定向过来
@app.route('/')
def hello_world():
    return 'Hello World!!!!!!!!'


# 将函数绑定到路径的第二种方式：
def hello_world2():
    return 'Hello World22222222'


app.add_url_rule(rule='/hello_world2', view_func=hello_world2)


@app.route('/user/<name>')
def user_page(name):
    """
    用户输入的数据会包含恶意代码，所以不能直接作为响应返回，需要使用 MarkupSafe（Flask 的依赖之一）提供的 escape() 函数对 name 变量进行转义处理，
    比如把 < 转换成 &lt;。这样在返回响应时浏览器就不会把它们当做代码执行。
    """
    return f'User: {escape(name)}'


@app.route('/test')
def test_url_for():
    # 下面是一些调用示例（请访问 http://localhost:5000/test 后在命令行窗口查看输出的 URL）：
    print(url_for(endpoint='hello_world'))  # 生成 hello_world 视图函数对应的 URL，将会输出：/
    # 注意下面两个调用是如何生成包含 URL 变量的 URL 的
    print(url_for(endpoint='user_page', name='greyli'))  # 输出：/user/greyli
    print(url_for(endpoint='user_page', name='peter'))  # 输出：/user/peter
    print(url_for(endpoint='test_url_for'))  # 输出：/test
    # 下面这个调用传入了多余的关键字参数，它们会被作为查询字符串附加到 URL 后面。
    print(url_for(endpoint='test_url_for', num=2))  # 输出：/test?num=2
    return 'Test page'


@app.errorhandler(404)  # 全局的404都会指向这里
def not_found(error):
    # return render_template('error.html'), 404
    return '404test'


@app.route('/api/test1', methods=['get'])
def hello_world1():
    return '/api/test1'


@app.route('/api/test2', methods=['post'])
def hello_world3():
    print({
        'request.host': request.host,
        'request.url': request.url,
        'request.remote_addr': request.remote_addr,
        'request.method': request.method,
        'request.args': request.args,
        'request.data': request.data,
        'request.headers': request.headers,
    })
    resp = {'hello': 'world', 'num': 111}
    return jsonify(resp)
    return json.dumps(resp)
    # return 响应体, 状态码, 响应头
    return res_json, 200, {"Content-Type": "application/json"}



if __name__ == '__main__':
    app.run()
