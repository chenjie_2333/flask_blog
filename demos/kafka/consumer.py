from kafka import KafkaConsumer
import json


def kafka_consumer():
    # 创建 KafkaConsumer 实例，指定 Kafka 服务器地址
    consumer = KafkaConsumer(
        "test_topic",  # 指定要消费的主题
        bootstrap_servers=["localhost:9092"],  # Kafka 服务器地址
        auto_offset_reset="earliest",  # 从最早的消息开始消费
        value_deserializer=lambda v: json.loads(v.decode("utf-8"))  # 使用 JSON 反序列化消息
    )

    print("Listening for messages on Kafka topic...")
    try:
        # 消费消息
        for message in consumer:
            print(f"Received message: {message.value}")
    except KeyboardInterrupt:
        print("Consumer stopped.")
    finally:
        consumer.close()


if __name__ == "__main__":
    kafka_consumer()
