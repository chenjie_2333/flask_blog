from kafka import KafkaProducer
import json
import time


def kafka_producer():
    # 创建 KafkaProducer 实例，指定 Kafka 服务器地址
    producer = KafkaProducer(
        bootstrap_servers=["localhost:9092"],  # Kafka 服务器地址
        value_serializer=lambda v: json.dumps(v).encode("utf-8")  # 使用 JSON 序列化消息
    )

    # 指定主题
    topic = "test_topic"

    # 发送消息
    for i in range(10):
        message = {"message_id": i, "content": f"Hello, Kafka! This is message {i}"}
        producer.send(topic, value=message)
        print(f"Sent message: {message}")
        time.sleep(1)  # 每秒发送一条消息

    # 确保所有消息都已发送
    producer.flush()
    print("All messages sent successfully.")


if __name__ == "__main__":
    kafka_producer()
