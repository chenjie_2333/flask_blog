# pip install kafka-python

# https://www.bilibili.com/video/BV1TT421y79S/?spm_id_from=333.337.search-card.all.click&vd_source=9a641ad443c2affe58b919899a5625a5
# https://blog.csdn.net/BigCookies/article/details/145442415

# 中数据的删除跟有没有消费者消费完全无关。数据的删除，只跟kafka broker上面上面的这两个配置有关：
# log.retention.hours=48 #数据最多保存48小时
# log.retention.bytes=1073741824 #数据最多1G
