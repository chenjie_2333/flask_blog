import socket


def tcp_server():
    # 创建一个TCP/IP套接字,socket.AF_INET表示使用IPv4地址，socket.SOCK_STREAM表示使用TCP协议。
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # 获取本地主机名
    host = socket.gethostname()

    # 设置端口号
    port = 9999

    # 绑定地址（主机名、端口号）
    server_socket.bind((host, port))

    # 设置最大连接数，超过后排队
    server_socket.listen(5)
    print(f"服务器启动，监听 {host}:{port}")

    while True:
        # 建立客户端连接
        client_socket, addr = server_socket.accept()  # 阻塞并等待客户端连接
        print(f"连接地址: {addr}")

        # 接收客户端消息
        data = client_socket.recv(1024).decode()  # 从客户端接收最多1024字节的数据
        print(f"收到客户端消息: {data}")

        # 向客户端发送消息
        message = "服务器已收到消息！"
        client_socket.send(message.encode())

        # 关闭客户端连接
        client_socket.close()


if __name__ == "__main__":
    tcp_server()