import socket


def tcp_client():
    # 创建一个TCP/IP套接字
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # 获取本地主机名
    host = socket.gethostname()

    # 设置端口号
    port = 9999

    # 连接到服务器
    client_socket.connect((host, port))

    # 向服务器发送消息
    message = "你好，服务器！"
    client_socket.send(message.encode())

    # 接收服务器响应
    data = client_socket.recv(1024).decode()
    print(f"收到服务器消息: {data}")

    # 关闭连接
    client_socket.close()


if __name__ == "__main__":
    tcp_client()