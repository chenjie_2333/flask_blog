import socket
import time


def tcp_server(file_size_mb=100):
    # 创建TCP套接字
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # 获取本地主机名
    host = socket.gethostname()

    # 设置端口号
    port = 9999

    # 绑定地址
    server_socket.bind((host, port))

    # 开始监听
    server_socket.listen(5)
    print(f"服务器启动，监听 {host}:{port}")

    # 接受客户端连接
    client_socket, addr = server_socket.accept()
    print(f"连接来自 {addr}")

    # 准备发送的数据（以MB为单位）
    data_to_send = b"0" * (file_size_mb * 1024 * 1024)  # 生成指定大小的数据

    # 开始发送数据
    print("开始发送数据...")
    start_time = time.time()
    client_socket.sendall(data_to_send)
    end_time = time.time()

    # 计算传输时间
    transfer_time = end_time - start_time
    print(f"数据发送完成，耗时 {transfer_time:.2f} 秒")

    # 关闭连接
    client_socket.close()
    server_socket.close()


if __name__ == "__main__":
    tcp_server()