import socket
import time


def tcp_client():
    # 创建TCP套接字
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # 获取本地主机名
    host = socket.gethostname()

    # 设置端口号
    port = 9999

    # 连接到服务器
    client_socket.connect((host, port))
    print(f"已连接到服务器 {host}:{port}")

    # 准备接收数据
    print("开始接收数据...")
    start_time = time.time()

    # 接收数据
    received_data = bytearray()
    while True:
        data = client_socket.recv(4096)
        if not data:
            break
        received_data.extend(data)

    end_time = time.time()

    # 计算传输时间
    transfer_time = end_time - start_time
    print(f"数据接收完成，耗时 {transfer_time:.2f} 秒")

    # 计算传输速度
    file_size_mb = len(received_data) / (1024 * 1024)
    transfer_speed = file_size_mb / transfer_time
    print(f"传输速度: {transfer_speed:.2f} MB/s")

    # 关闭连接
    client_socket.close()


if __name__ == "__main__":
    tcp_client()