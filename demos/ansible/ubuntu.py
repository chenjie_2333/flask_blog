# linux ubuntu docker下载并配置ssh
# https://blog.csdn.net/qq_44667259/article/details/129867255
# docker run -itd -p 0.0.0.0:10000:22 --name myubuntu ubuntu:22.04 /bin/bash  （ssh需要使用22端口）
# docker run -itd -p 0.0.0.0:10001:22 --name ubuntu_local ubuntu:22.04 /bin/bash
# ssh -p 10000 root@192.168.72.1


# VM环境准备  https://blog.csdn.net/qq_44667259/article/details/129867255
# apt update
# apt install openssh-server
# apt install vim


# apt install ansible
# # https://blog.csdn.net/leah126/article/details/138302675
# https://www.cnblogs.com/yanjieli/p/10969089.html
# 1.概念定义：
# Ansible：ansible核心程序
# Host Inventory：主机清单，可以定义主机组和主机
# 模块：ansible执行任务是由模块执行的，不是ansible它自己执行的
# Playbook：剧本，Yaml定义的文件，类似于shell脚本
# Connect Plugin：连接插件
# 2.原理：
# 我们的控制端一般拥有多个被控端，当我么需批量的去对主机进行操作的时候，这时候我们可以通过inventory定义主机组和主机，安装的时候，我们可以编写playbook或者AD-HOC，然后把模块等推给被控端，然后被控端再执行playbook或者AH-HOC，完成任务，这个推送过程是使用ssh的方式推送的，确保安全，并且客户端必须有Python2，确保能够执行playbook或者AD-HOC。这个就是他的整个架构原理
# 3.命令执行过程
# 加载自己的配置文件，默认/etc/ansible/ansible.cfg；
# 查找对应的主机配置文件，找到要执行的主机或者组；
# 加载自己对应的模块文件，如 command；
# 通过ansible将模块或命令生成对应的临时py文件(python脚本)， 并将该文件传输至远程服务器；
# 对应执行用户的家目录的.ansible/tmp/XXX/XXX.PY文件；
# 给文件 +x 执行权限；
# 执行并返回结果；
# 删除临时py文件，sleep 0退出；
# 4.特性
# no agents：不需要在被管控主机上安装任何客户端；
# no server：无服务器端，使用时直接运行命令即可；
# modules in any languages：基于模块工作，可使用任意语言开发模块；
# yaml，not code：使用yaml语言定制剧本playbook；
# ssh by default：基于SSH工作；
# strong multi-tier solution：可实现多级指挥。


# todo 环境问题处理不了，等用腾讯云的虚拟机试试
