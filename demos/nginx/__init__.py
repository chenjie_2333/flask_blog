# sudo systemctl restart nginx
# nginx -t  # 测试配置文件语法是否正确

# 后端服务器的配置：
# 确保后端服务器能够正确处理来自 Nginx 的请求，例如解析 X-Forwarded-For 头以获取客户端的真实 IP。
# 防火墙和端口：
# 确保 Nginx 服务器可以访问后端服务器的指定端口。
# 性能优化：
# 可以通过调整 worker_processes 和 worker_connections 参数优化 Nginx 的性能。
# 会话保持：
# 如果需要会话保持，可以使用 ip_hash 或其他会话保持机制。

