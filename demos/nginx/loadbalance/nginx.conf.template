# 定义后端服务器组
upstream backend_servers {
    # 默认为轮询
    server backend1.example.com:8080;  # 后端服务器1
    server backend2.example.com:8080;  # 后端服务器2
    server backend3.example.com:8080;  # 后端服务器3
}

server {
    listen 80;
    server_name example.com;

    location / {
        proxy_pass http://backend_servers;  # 转发到后端服务器组
        proxy_set_header Host $host;        # 保留原始请求的 Host 头
        proxy_set_header X-Real-IP $remote_addr;  # 传递客户端的真实 IP
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;  # 传递客户端的真实 IP
        proxy_set_header X-Forwarded-Proto $scheme;  # 传递原始请求的协议
    }
}

# ---------------------------------------------------------------------------
# 其他负载均衡策略
# 1. 最少连接法
upstream backend_servers {
    least_conn;  # 启用最少连接法
    server backend1.example.com:8080;
    server backend2.example.com:8080;
    server backend3.example.com:8080;
}
# 2. 权重法
upstream backend_servers {
    server backend1.example.com:8080 weight=3;  # 权重为3
    server backend2.example.com:8080 weight=2;  # 权重为2
    server backend3.example.com:8080 weight=1;  # 权重为1
}
# 3. ip hash  根据客户端的 IP 地址进行哈希，确保同一个客户端的请求总是被转发到同一个后端服务器。适合需要会话保持的场景
upstream backend_servers {
    ip_hash;  # 启用IP哈希法
    server backend1.example.com:8080;
    server backend2.example.com:8080;
    server backend3.example.com:8080;
}