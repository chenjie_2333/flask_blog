from urllib.request import urlopen


def baidu():
    # 获取整个页面
    resp = urlopen("http://www.baidu.com")  # python原生的http库
    print(resp.read().decode('utf-8'))
    with open("baidu.html", "w", encoding='utf-8') as f:
        f.write(resp.read().decode('utf-8'))