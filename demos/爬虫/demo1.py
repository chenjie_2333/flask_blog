# pip install requests
# pip install parsel
# pip install pyexecjs
import parsel as parsel
import requests


def tonghuashun():
    url = 'https://q.10jqka.com.cn/index/index/board/all/field/zdf/order/desc/page/1/ajax/1/'
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/134.0.0.0 Safari/537.36 Edg/134.0.0.0',
        'Cookie': 'log=; Hm_lvt_722143063e4892925903024537075d0d=1742120757; Hm_lpvt_722143063e4892925903024537075d0d=1742120757; HMACCOUNT=3DA03BDCC800F0AF; Hm_lvt_929f8b362150b1f77b477230541dbbc2=1742120757; Hm_lpvt_929f8b362150b1f77b477230541dbbc2=1742120757; Hm_lvt_78c58f01938e4d85eaf619eae71b4ed1=1742120757; Hm_lpvt_78c58f01938e4d85eaf619eae71b4ed1=1742120770; __utma=156575163.1638577496.1742120770.1742120770.1742120770.1; __utmc=156575163; __utmz=156575163.1742120770.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); __utmt=1; __utmb=156575163.1.10.1742120770; v=A5S-tIw-j5EzCRtdeRuilbVRZdkD7btn-hVMGy51IMCi8zrHVv2IZ0ohHKN9',
    }
    response = requests.get(url, headers=headers)
    _html = response.text
    # print(_html)
    # 把获取到的html字符串转换为Selector对象
    selector = parsel.Selector(_html)
    # 提取所有的tr标签
    # trs = selector.css('.m-table.m-pager-table tr')[1:]  # 先找到m-table.m-pager-table标签， 再找到tr标签
    trs = selector.css('.m-table tr')[1:]  # 简写
    titles = selector.css('.m-table tr')[0]
    page_data = []
    for tr in trs:
        info = tr.css('td::text').getall()  # 取td标签中的文本
        info_a = tr.css('td a::text').getall()  # 取td标签下a标签中的文本
        info = info[:1] + info_a + info[1:]
        info_title = titles.css('th::text').getall()
        info_title_a = titles.css('th a::text').getall()
        info_title = info_title + info_title_a
        cur = dict(zip(info_title, info))
        print(cur)
        page_data.append(cur)


if __name__ == '__main__':
    tonghuashun()
