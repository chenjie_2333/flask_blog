import requests


# 为什么使用 requests.Session？
# 保持会话状态：
# Session 对象可以自动处理 cookies，保持会话状态。这对于需要登录或需要跨多个请求保持状态的场景非常有用。
# 性能优化：
# Session 使用连接池，可以复用连接，减少连接开销，提高性能。
# 代码复用：
# 可以在多个请求中复用配置（如 headers、proxies 等），使代码更简洁。

def t_simple():
    # 创建一个 Session 对象
    session = requests.Session()
    # 设置通用的请求头
    session.headers.update({
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64)",
        "Accept-Language": "en-US,en;q=0.9"
    })
    # 发送 GET 请求
    response = session.get("https://httpbin.org/get")
    print(response.json())
    # 发送 POST 请求
    data = {"key": "value"}
    response = session.post("https://httpbin.org/post", data=data)
    print(response.json())
    # 关闭 Session
    session.close()


def t_login_cookie():
    # 登录并保持会话
    # Session 对象可以自动处理 cookies，这对于需要登录的场景非常有用。
    session = requests.Session()
    login_url = "https://example.com/login"
    login_data = {
        "username": "your_username",
        "password": "your_password"
    }
    # 发送登录请求
    response = session.post(login_url, data=login_data)
    # 检查是否登录成功
    if response.ok:
        print("登录成功！")
        # 访问需要登录的页面
        response = session.get("https://example.com/dashboard")
        print(response.text)
    else:
        print("登录失败！")
    # 关闭 Session
    session.close()


def t_multi():
    # Session 使用连接池来复用 TCP 连接，提高性能。这对于需要频繁请求同一服务器的场景非常有效。
    # 创建 Session 对象
    session = requests.Session()
    # 发送多个请求
    for i in range(5):
        response = session.get("https://httpbin.org/get")
        print(response.json())
    # 关闭 Session
    session.close()


def t_with():
    # 为了避免忘记关闭 Session，可以使用上下文管理器（with 语句）
    # 使用上下文管理器创建 Session
    with requests.Session() as session:
        # 设置通用请求头
        session.headers.update({
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64)"
        })

        # 发送请求
        response = session.get("https://httpbin.org/get")
        print(response.json())

    # 退出上下文时，Session 自动关闭
